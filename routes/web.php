<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');

Route::resource('users', 'UsersController');
Route::resource('cathegories', 'CathegoriesController');
Route::resource('products', 'ProductsController');

/*
Route::get('/user', function () {
    return view('user');
});

Route::get('/user/{id}', function ($id) {
    return view('showUser'); //Show
})->where('id', '[0-9]+');

Route::get('/user/new', function () {
    return view('newUser'); //create
});*/