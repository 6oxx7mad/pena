@extends('template.layout')

@section('title', 'Pena')

@section('content')

    <h1>Alta de Producto</h1>
    
    <form method="POST" action="/products">
        {{ csrf_field() }}

        <div>
            <input type="text" name="name" placeholder="Product Name">
        </div>

        <div>
            <input type="number" name="price" placeholder="Product Price">
        </div>

        <select name="cathegory_id">
        @foreach ($cathegories as $cathegory)
            <option value="{{ $cathegory->id }}"
            }}>{{ $cathegory->name }}
        </option>
        @endforeach
        </select>
        
        <div>
            <button type="submit">Create Product</button>
        </div>
    </form>
@endsection
