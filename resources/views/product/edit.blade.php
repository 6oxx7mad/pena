@extends('template.layout')


@section('title', 'Pena')


@section('content')

    <h1 class="title">Edición de {{ $product->name }}</h1>
    
    <form method="POST" action="/products/{{ $product->id }}" style="margin-bottom: 1rem;">

        @method('PATCH');


        @csrf

        <div class="field">

            <label class="label" for="name">Name:</label>

            <div class="control">

                <input type="text" name="name" placeholder="name" value="{{ $product->name }}">

            </div>

        </div>

        <div class="field">

            <label class="label" for="price">Price:</label>

            <div class="control">

                <input type="number" name="price" placeholder="price" value="{{ $product->price }}">

            </div>

        </div>

        <div class="field">

            <div class="control">

                <div class="select is-primary">

                    <select name="cathegory_id">


                    @foreach ($cathegories as $cathegory)

                        <option value="{{ $cathegory->id }}" 
                                    
                            {{ $cathegory->id == $product->cathegory_id ? 'selected="selected"' : '' }}
                            >{{ $cathegory->name }}

                        </option>

                    @endforeach


                    </select>

                </div>

            </div>

        </div>
        
        <div class="field">

            <div class="control">

                <button type="submit" class="button is-link">Edit</button>

            </div>  

        </div>

    </form>

    <form method="POST" action="/products/{{ $product->id }}">

        @method('DELETE')


        @csrf

        <div class="field">

            <div class="control">

                <button type="submit" class="button is-danger">Delete</button>

            </div>  

        </div>

    </form>


@endsection
