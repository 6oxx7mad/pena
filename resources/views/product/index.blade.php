@extends('template.layout')

@section('title', 'Pena')

@section('content')

    <h1>Productos</h1>
    
    @foreach ($products as $product)
        <li>{{ $product->name }}</li>
    @endforeach

@endsection
