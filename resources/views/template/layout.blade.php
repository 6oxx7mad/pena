<!DOCTYPE html>
<html lang="es">
<head>
    <title>@yield('title', 'Pena Edition')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css" />
</head>
<body>
    
    <ul>
        <li><a href="/">Inicio</a></li>
        <li><a href="/users">Usuarios</a>.</li>
        <li><a href="/products">Productos</a></li>
    </ul>
    @yield('content')
</body>
</html>