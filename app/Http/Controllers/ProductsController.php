<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product.index', compact('products'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cathegories = Cathegory::all();
        return view('product.create', compact('cathegories'));
    }

    public function store(Request $request)
    {
        /*$rules = ['name' => 'required|max:255|min:4', 'price' => 'required|numeric', 'cathegory_id' => 'min:1'];
        $request->validate($rules);*/
        $product = new Product();
        
        $product->name = request('name');
        $product->price = request('price');
        $product->cathegory_id = request('cathegory_id');

        $product->save();
        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $product = Product::findOrFail($id);
        
        return view('product.show', compact('product'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = Product::findOrFail($id);
        $cathegories = Cathegory::all();

        return view('product.edit', compact('product', 'cathegories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = ['name' => 'required|max:255|min:4', 'price' => 'required|numeric', 'cathegory_id' => 'min:1'];
        $request->validate($rules);
        $product = Product::findOrFail($id);
        $product->name = request('name');
        $product->price = request('price');
        $product->cathegory_id = request('cathegory_id');
        $product->save();
        return redirect('/products');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Product::find($id)->delete();

        return redirect('/products');

    }
}
