<?php

namespace App\Http\Controllers;

use App\Cathegory;
use Illuminate\Http\Request;

class CathegoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cathegory  $cathegory
     * @return \Illuminate\Http\Response
     */
    public function show(Cathegory $cathegory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cathegory  $cathegory
     * @return \Illuminate\Http\Response
     */
    public function edit(Cathegory $cathegory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cathegory  $cathegory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cathegory $cathegory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cathegory  $cathegory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cathegory $cathegory)
    {
        //
    }
}
